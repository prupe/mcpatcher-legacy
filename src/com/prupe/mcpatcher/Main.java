package com.prupe.mcpatcher;

import java.io.File;

public final class Main {
    private Main() {
    }

    public static void main(String[] args) {
        int exitStatus = 1;
        try {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-export") && i + 1 < args.length) {
                    i++;
                    exitStatus = export(args[i]);
                } else if (args[i].equals("-getversion") && i + 1 < args.length) {
                    i++;
                    exitStatus = getVersion(args[i]);
                } else {
                    System.err.printf("ERROR: unknown argument %s\n", args[i]);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        System.exit(exitStatus);
    }

    private static int export(String path) {
        LegacyVersionList list = getVersionList();

        LegacyVersionList.Version mods2 = new LegacyVersionList.Version("mods2", MCPatcher.API_VERSION, "13w01b", "2.4.6");
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.BASE_TEXTURE_PACK_MOD, "com.prupe.mcpatcher.mod.BaseTexturePackMod").setInternal(true));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.HD_TEXTURES, "com.prupe.mcpatcher.mod.HDTexture"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.HD_FONT, "com.prupe.mcpatcher.mod.HDFont"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.BETTER_GRASS, "com.prupe.mcpatcher.mod.BetterGrass"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.RANDOM_MOBS, "com.prupe.mcpatcher.mod.RandomMobs"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.CUSTOM_COLORS, "com.prupe.mcpatcher.mod.CustomColors"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.CONNECTED_TEXTURES, "com.prupe.mcpatcher.mod.ConnectedTextures"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.BETTER_GLASS, "com.prupe.mcpatcher.mod.BetterGlass"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.BETTER_SKIES, "com.prupe.mcpatcher.mod.BetterSkies"));
        mods2.add(new LegacyVersionList.Mod(MCPatcherUtils.GLSL_SHADERS, "com.prupe.mcpatcher.mod.GLSLShader").setExperimental(true));
        mods2.md5 = Util.computeMD5(new File("out/artifacts/mods2/mods2.jar"));
        list.add(mods2);

        LegacyVersionList.Version mods3 = new LegacyVersionList.Version("mods3", MCPatcher.API_VERSION, "13w17a", "3.0.5_01");
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.BASE_TEXTURE_PACK_MOD, "com.prupe.mcpatcher.mod.BaseTexturePackMod").setInternal(true));
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.EXTENDED_HD, "com.prupe.mcpatcher.mod.ExtendedHD"));
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.HD_FONT, "com.prupe.mcpatcher.mod.HDFont"));
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.RANDOM_MOBS, "com.prupe.mcpatcher.mod.RandomMobs"));
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.CUSTOM_COLORS, "com.prupe.mcpatcher.mod.CustomColors"));
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.CONNECTED_TEXTURES, "com.prupe.mcpatcher.mod.ConnectedTextures"));
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.BETTER_GLASS, "com.prupe.mcpatcher.mod.BetterGlass"));
        mods3.add(new LegacyVersionList.Mod(MCPatcherUtils.BETTER_SKIES, "com.prupe.mcpatcher.mod.BetterSkies"));
        mods3.md5 = Util.computeMD5(new File("out/artifacts/mods3/mods3.jar"));
        list.add(mods3);

        JsonUtils.writeJson(list, new File(path));
        return 0;
    }

    private static int getVersion(String id) {
        LegacyVersionList list = getVersionList();
        for (LegacyVersionList.Version version : list.find(MCPatcher.API_VERSION)) {
            if (id.equals(version.id)) {
                System.out.println(version.libraryVersion);
                return 0;
            }
        }
        System.err.printf("ERROR: %s not found\n", id);
        return 1;
    }

    private static LegacyVersionList getVersionList() {
        File file = new File(LegacyVersionList.VERSIONS_JSON);
        LegacyVersionList list = null;
        if (file.isFile()) {
            list = JsonUtils.parseJson(file, LegacyVersionList.class);
        }
        return list == null ? new LegacyVersionList() : list;
    }
}
