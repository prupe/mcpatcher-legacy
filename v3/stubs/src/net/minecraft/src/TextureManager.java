package net.minecraft.src;

import java.awt.image.BufferedImage;
import java.util.List;

public class TextureManager {
    public static TextureManager getInstance() {
        return null;
    }

    public Texture createTextureFromImage(String name, int slot, int width, int height, int wrapST, int pixelFormat, int minFilter, int magFilter, boolean flag, BufferedImage image) {
        return null;
    }

    public List<Texture> createTextureFromFile(String filename) {
        return null;
    }
}
